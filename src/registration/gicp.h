#pragma once

#include <Eigen/Geometry>
#include <sophus/se3.hpp>
#include <sophus/types.hpp>
#include <sophus/common.hpp>
#include <pcl/point_cloud.h>
#include <pcl/kdtree/kdtree_flann.h>


namespace perl_registration {
    template <typename PointT>
    class GICP
    {
        public:
        typedef pcl::PointCloud<PointT> PointCloud;
        typedef typename PointCloud::Ptr PointCloudPtr;

        typedef std::vector< Eigen::Matrix3d, Eigen::aligned_allocator<Eigen::Matrix3d> > MatricesVector;
        typedef std::vector< Eigen::Matrix<double,6,6>,
                             Eigen::aligned_allocator<Eigen::Matrix<double,6,6> > >
                                 CovarianceVector;

        typedef std::shared_ptr< MatricesVector > MatricesVectorPtr;
        typedef std::shared_ptr< const MatricesVector > MatricesVectorConstPtr;

        typedef pcl::KdTreeFLANN<PointT> KdTree;
        typedef typename KdTree::Ptr KdTreePtr;

        typedef Eigen::Matrix<double, 6, 1> Vector6d;

        std::vector< Eigen::Matrix4f, Eigen::aligned_allocator<Eigen::Matrix4f> > convergeTransforms;
        std::vector< double > convergeTime;

        GICP(int k = 20, double epsilon = 0.001) :
        kCorrespondences_(k),
        epsilon_(epsilon)
        {
            Eigen::Matrix4d mat = Eigen::Matrix4d::Identity();
            baseTransformation_ = Sophus::SE3d(mat);
        };

        inline void
        SetInputSource( const PointCloudPtr &cloud ) {
            sourceCloud_ = cloud;
            sourceKdTree_ = KdTreePtr(new KdTree());
            sourceKdTree_->setInputCloud(sourceCloud_);
            sourceCovariances_ = MatricesVectorPtr(new MatricesVector());
        };

        inline void
        SetInputTarget ( const PointCloudPtr &cloud ) {
            targetCloud_ = cloud;
            targetKdTree_ = KdTreePtr(new KdTree());
            targetKdTree_->setInputCloud(targetCloud_);
            targetCovariances_ = MatricesVectorPtr(new MatricesVector());
        };


        void
        Align(PointCloudPtr finalCloud);

        void
        Align(PointCloudPtr finalCloud, const Sophus::SE3d &initTransform);

        Sophus::SE3d
        GetFinalTransFormation() {
            Sophus::SE3d temp = finalTransformation_;
            return temp;
        };

        int
        GetOuterIter(){
            return outer_iter;
        }

        protected:

        int kCorrespondences_;
        double epsilon_;
        double translationEpsilon_;
        double rotationEpsilon_;
        int maxInnerIterations_;

        int outer_iter;

        Sophus::SE3d baseTransformation_;
        Sophus::SE3d finalTransformation_;

        PointCloudPtr sourceCloud_;
        KdTreePtr sourceKdTree_;
        MatricesVectorPtr sourceCovariances_;

        PointCloudPtr targetCloud_;
        KdTreePtr targetKdTree_;
        MatricesVectorPtr  targetCovariances_;

        void ComputeCovariances(const PointCloudPtr cloudptr, KdTreePtr treeptr, MatricesVectorPtr matvec);

    };
} // namespace perl_registration

#include "registration/impl/gicp.hpp"

