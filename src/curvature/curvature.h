#pragma once

#include <Eigen/Core>

#include <sophus/common.hpp>
#include <sophus/se3.hpp>
#include <sophus/types.hpp>

#include "ceres/ceres.h"

#include "cukdtree/cukdtree.h"
#include "cupointcloud/cupointcloud.h"

namespace perl_registration {

class Curvature {
 public:
  typedef cuPointXYZ PointType;
  typedef std::shared_ptr<Curvature> SharedPtr;
  typedef std::shared_ptr<const Curvature> SharedConstPtr;

  typedef Eigen::Matrix3f CovarianceType;
  typedef typename thrust::device_vector<CovarianceType> CovarianceVector;
  typedef typename std::shared_ptr<CovarianceVector> CovarianceVectorPtr;

  struct Shell {
    Eigen::Vector3f param;
    Eigen::Vector3f centroid;
    float radius;
    float extent;
  };

  Curvature() : k_correspondences_(24) {}

  inline void SetInputSource(const cuPointCloud<PointType>::SharedPtr &cloud) {
    d_source_cloud_ = cloud;
    d_source_kd_tree_ = nullptr;
    d_source_covariances_ = nullptr;
  }

  inline void SetInputSource(const cuPointCloud<PointType>::SharedPtr &cloud,
                             const cuKdTree<PointType>::SharedPtr &kdtree,
                             const CovarianceVectorPtr &covariances) {
    d_source_cloud_ = cloud;
    d_source_kd_tree_ = kdtree;
    d_source_covariances_ = covariances;
  }

  inline void GetSourceVectors(cuKdTree<PointType>::SharedPtr &kdtree,
                               CovarianceVectorPtr &covariances) {
    kdtree = d_source_kd_tree_;
    covariances = d_source_covariances_;
  }

  inline void SetKCorrespondences(const size_t k) { k_correspondences_ = k; }

  inline size_t GetKCorrespondences() { return k_correspondences_; }

  void Compute(cuPointCloud<cuPointXYZI>::SharedPtr out);

 private:
  cuPointCloud<PointType>::SharedPtr d_source_cloud_;
  cuKdTree<PointType>::SharedPtr d_source_kd_tree_;
  CovarianceVectorPtr d_source_covariances_;

  size_t k_correspondences_;
};

}  // namespace perl_registration
