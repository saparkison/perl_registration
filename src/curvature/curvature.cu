#include "curvature.h"

#include <Eigen/Eigenvalues>

#include <thrust/device_vector.h>
#include <thrust/functional.h>
#include <thrust/transform.h>

#include "common/gpuutil.h"
#include "common/inverse.h"
#include "common/local_parameterization_se3.h"

#define THREADNUM 128

namespace perl_registration {

__global__ void ComputeNorms(KernelArray<cuPointXYZ> points,
                             KernelArray<int> k_nns,
                             KernelArray<Eigen::Matrix3f> covariances,
                             KernelArray<Eigen::Vector3f> norms, int k) {
  int pos = blockIdx.x * blockDim.x + threadIdx.x;
  if (pos >= points.size) return;

  Eigen::Vector3f mean(0, 0, 0);
  Eigen::Matrix3f covariance = Eigen::Matrix3f::Zero();

  for (int i = pos * k; i < (pos + 1) * k; i++) {
    mean = mean + points.data[k_nns.data[i]].toVec();
  }
  mean = mean * (1.0f / (float)(k));

  for (int i = pos * k; i < (pos + 1) * k; i++) {
    Eigen::Vector3f temp = points.data[k_nns.data[i]].toVec() - mean;
    Eigen::Matrix3f temp_m = temp * temp.transpose();
    covariance = covariance + temp_m;
  }
  covariance = covariance * (1.0f / (float)(k - 1));

  /* PCA */
  Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> es(3);
  es.computeDirect(covariance);
  // Eigen values are sorted
  Eigen::Matrix3f eigen_value_replacement = Eigen::Matrix3f::Zero();
  eigen_value_replacement(0, 0) = 1e-4;
  eigen_value_replacement(1, 1) = 1;
  eigen_value_replacement(2, 2) = 1;
  covariances.data[pos] = es.eigenvectors() * eigen_value_replacement *
                          es.eigenvectors().transpose();
  covariance = covariances.data[pos];

  // Generate rotation (as a homegenous transform) to align
  //  the norm with x axis and y with principal_axis
  //  This can also be seen as a change of basis from standard basis to
  //     b = {norm, principal_axis, norm X principal_axis}
  //  Changing the basis of the points to b and setting the component
  //  corresponding to the
  //    principal_axis to zero yields the projection onto the plane that has
  //    the
  //    principal_axis as its norm
  Eigen::Vector3f norm = es.eigenvectors().col(0);
  norms.data[pos] = norm;
}

__global__ void ComputeCurvature(KernelArray<cuPointXYZ> points,
                                 KernelArray<int> k_nns,
                                 KernelArray<Eigen::Matrix3f> covariances,
                                 KernelArray<Eigen::Vector3f> norms,
                                 KernelArray<cuPointXYZI> out_points, int k) {
  int pos = blockIdx.x * blockDim.x + threadIdx.x;
  if (pos >= points.size) return;

  Eigen::Matrix3f projection = Eigen::Matrix3f::Identity() -
                               norms.data[pos] * norms.data[pos].transpose();
  Eigen::Vector3f norm_mean = Eigen::Vector3f::Zero();
  Eigen::Matrix3f norm_covariance = Eigen::Matrix3f::Zero();

  for (int i = pos * k; i < (pos + 1) * k; i++) {
    norm_mean = norm_mean + projection * norms.data[k_nns.data[i]];
  }
  norm_mean = norm_mean * (1.0f / (float)(k));

  for (int i = pos * k; i < (pos + 1) * k; i++) {
    Eigen::Vector3f temp = projection * norms.data[k_nns.data[i]] - norm_mean;
    Eigen::Matrix3f temp_m = temp * temp.transpose();
    norm_covariance = norm_covariance + temp_m;
  }
  norm_covariance = norm_covariance * (1.0f / (float)(k - 1));

  /* PCA */
  Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> es(3);
  es.computeDirect(norm_covariance);

  Eigen::Vector3f norm = norms.data[pos];
  Eigen::Vector3f principal_axis = es.eigenvectors().col(2);
  Eigen::Matrix3f rot;
  rot.block<1, 3>(0, 0) = norm;
  rot.block<1, 3>(1, 0) = principal_axis;
  rot.block<1, 3>(2, 0) = norm.cross(principal_axis);

  // Covariance like matrix of points if the mean
  // were 0,0,0
  //  var(x), cov(x,z), 0
  //  cov(x,z), var(z), 0
  //    0,        0,  neighborhood->size()
  Eigen::Matrix3f Q = Eigen::Matrix3f::Zero();

  float sum_y = 0;
  float min_y = -100000;
  float max_y = 100000;
  Eigen::Vector3f weighted_sum = Eigen::Vector3f::Zero();
  for (int i = pos * k; i < (pos + 1) * k; i++) {
    // Construct point
    Eigen::Vector3f rotated_point = rot * points.data[k_nns.data[i]].toVec();
    Eigen::Vector3f temp(rotated_point.x(), rotated_point.z(), 1);
    Eigen::Matrix3f temp_m = temp * temp.transpose();
    Q = Q + temp_m;

    // Construct sum of squares
    float sum_squares = rotated_point.x() * rotated_point.x() +
                        rotated_point.z() * rotated_point.z();

    // Accumulate wieghted sum
    weighted_sum += sum_squares * temp;

    // Accumulate ys
    sum_y += rotated_point.y();

    // Get min and max y
    if (rotated_point.y() < min_y) {
      min_y = rotated_point.y();
    } else if (rotated_point.y() > max_y) {
      max_y = rotated_point.y();
    }
  }

  // Solve Q * params = weighted_sum
  //  Since Q will be positive semi definite by
  //  construction we can use ldlt solver
  //  which is fast for large and small matrices
  //  and accurate
  // TODO(Kevin): Explain what params are and
  // why theres a -1
  Q = Q + Eigen::Matrix3f::Identity() * 0.001;
  Eigen::Matrix3f inv_Q = Inverse(Q);
  Eigen::Vector3f params = -1 * inv_Q * weighted_sum;
  //  params = -1 *
  //  Q.partialPivLu().solve(weighted_sum);

  // FOR  MATRIX INVERSE ERROR  CHECKING
  float inv_error = (Q * params + weighted_sum).norm() / weighted_sum.norm();

  Curvature::Shell shell;
  float avg_y = sum_y / (float)k;
  Eigen::Vector3f tfed_centroid;
  tfed_centroid(0) = -0.5 * params(0);
  tfed_centroid(1) = avg_y;
  tfed_centroid(2) = -0.5 * params(1);
  shell.centroid = rot.transpose() * tfed_centroid;
  shell.radius =
      sqrt(0.25f * (params(0) * params(0) + params(1) * params(1)) - params(2));
  shell.extent = max_y - min_y;
  shell.param = params;

  // if(isnan(shell.radius)) {
  if (pos == 2000) {
    printf("Eigen Values %f %f %f\n", es.eigenvalues()(0), es.eigenvalues()(1),
           es.eigenvalues()(2));
    printf("Radius %f index %d inv error %f \n", shell.radius, pos, inv_error);
    printf("Q\n %f %f %f\n %f %f %f\n %f %f %f\n", Q(0, 0), Q(0, 1), Q(0, 2),
           Q(1, 0), Q(1, 1), Q(1, 2), Q(2, 0), Q(2, 1), Q(2, 2));
    printf("Inv Q\n %f %f %f\n %f %f %f\n %f %f %f\n", inv_Q(0, 0), inv_Q(0, 1),
           inv_Q(0, 2), inv_Q(1, 0), inv_Q(1, 1), inv_Q(1, 2), inv_Q(2, 0),
           inv_Q(2, 1), inv_Q(2, 2));
    printf("params %f %f %f\n", params(0), params(1), params(2));
    printf("Weight sum %f %f %f\n", weighted_sum(0), weighted_sum(1),
           weighted_sum(2));
  }
  out_points.data[pos] = cuPointXYZI(points.data[pos].x, points.data[pos].y,
                                     points.data[pos].z, es.eigenvalues()(2));

  return;
}

void Curvature::Compute(cuPointCloud<cuPointXYZI>::SharedPtr out) {
  // std::cout << "computing source covs\n";
  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  cudaEventRecord(start);
  d_source_kd_tree_ = std::make_shared<cuKdTree<cuPointXYZ>>();
  d_source_kd_tree_->SetInputCloud(d_source_cloud_);
  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  float milliseconds;
  cudaEventElapsedTime(&milliseconds, start, stop);
  std::cout << "Kd Tree Elapsed time: " << milliseconds << std::endl;

  cudaEventRecord(start);
  out->points.resize(d_source_cloud_->size());
  thrust::device_vector<Eigen::Vector3f> norms(d_source_cloud_->size());
  thrust::device_vector<int> indices(d_source_cloud_->size() *
                                     k_correspondences_);
  d_source_kd_tree_->NearestKSearch(d_source_cloud_, k_correspondences_,
                                    indices);

  d_source_covariances_ =
      std::make_shared<CovarianceVector>(d_source_cloud_->size());

  int blocks = ceil((float)d_source_cloud_->size() / THREADNUM);
  ComputeNorms<<<blocks, THREADNUM>>>(d_source_cloud_->points, indices,
                                      *d_source_covariances_, norms,
                                      k_correspondences_);
  ComputeCurvature<<<blocks, THREADNUM>>>(d_source_cloud_->points, indices,
                                          *d_source_covariances_, norms,
                                          out->points, k_correspondences_);
  cudaSafe(cudaDeviceSynchronize());
  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&milliseconds, start, stop);
  std::cout << "Covariance Elapsed time: " << milliseconds << std::endl;
}

}  // namespace perl_registration
