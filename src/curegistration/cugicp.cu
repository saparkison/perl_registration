#include "cugicp.h"

#include <Eigen/Eigenvalues>

#include <thrust/device_vector.h>
#include <thrust/functional.h>
#include <thrust/transform.h>

#include "common/gpuutil.h"
#include "common/inverse.h"
#include "common/local_parameterization_se3.h"

#define THREADNUM 128

namespace perl_registration {

struct CostStruct {
  float cost;
  float gradient[7];

  //  __host__ __device__
  //  CostStruct() {
  //    cost = 0;
  //    gradient[0] = 0;
  //    gradient[1] = 0;
  //    gradient[2] = 0;
  //    gradient[3] = 0;
  //    gradient[4] = 0;
  //    gradient[5] = 0;
  //    gradient[6] = 0;
  //  }
};

__host__ __device__ CostStruct operator+(const CostStruct &a,
                                         const CostStruct &b) {
  CostStruct out;
  out.cost = a.cost + b.cost;
  out.gradient[0] = a.gradient[0] + b.gradient[0];
  out.gradient[1] = a.gradient[1] + b.gradient[1];
  out.gradient[2] = a.gradient[2] + b.gradient[2];
  out.gradient[3] = a.gradient[3] + b.gradient[3];
  out.gradient[4] = a.gradient[4] + b.gradient[4];
  out.gradient[5] = a.gradient[5] + b.gradient[5];
  out.gradient[6] = a.gradient[6] + b.gradient[6];
  return out;
};

struct GICPCostFunctor {
  bool compute_jacobian;
  float b;
  float c;
  Eigen::Matrix3f R;
  Eigen::Matrix3f dRdw;
  Eigen::Matrix3f dRdx;
  Eigen::Matrix3f dRdy;
  Eigen::Matrix3f dRdz;

  __host__ __device__ GICPCostFunctor(const float &alpha_,
                                      const bool &compute_jacobian_,
                                      Eigen::Quaternionf q)
      : compute_jacobian(compute_jacobian_), b(alpha_ * alpha_), c(1.f / b) {
    R = q.toRotationMatrix();
    const float tx = float(2) * q.x();
    const float ty = float(2) * q.y();
    const float tz = float(2) * q.z();
    const float tw = float(2) * q.w();
    const float mfx = float(-2) * tx;
    const float mfy = float(-2) * ty;
    const float mfz = float(-2) * tz;
    const float mtw = float(-1) * tw;
    /* Eigen Quat to Rot
    res.coeffRef(0,0) = Scalar(1)-(tyy+tzz);
    res.coeffRef(0,1) = txy-twz;
    res.coeffRef(0,2) = txz+twy;
    res.coeffRef(1,0) = txy+twz;
    res.coeffRef(1,1) = Scalar(1)-(txx+tzz);
    res.coeffRef(1,2) = tyz-twx;
    res.coeffRef(2,0) = txz-twy;
    res.coeffRef(2,1) = tyz+twx;
    res.coeffRef(2,2) = Scalar(1)-(txx+tyy);
    */
    dRdw(0, 0) = float(0);
    dRdw(0, 1) = -tz;
    dRdw(0, 2) = ty;
    dRdw(1, 0) = tz;
    dRdw(1, 1) = float(0);
    dRdw(1, 2) = -tx;
    dRdw(2, 0) = -ty;
    dRdw(2, 1) = tx;
    dRdw(2, 2) = float(0);

    dRdx(0, 0) = float(0);
    dRdx(0, 1) = ty;
    dRdx(0, 2) = tz;
    dRdx(1, 0) = ty;
    dRdx(1, 1) = mfx;
    dRdx(1, 2) = mtw;
    dRdx(2, 0) = tz;
    dRdx(2, 1) = tw;
    dRdx(2, 2) = mfx;

    dRdy(0, 0) = mfy;
    dRdy(0, 1) = tx;
    dRdy(0, 2) = tw;
    dRdy(1, 0) = tx;
    dRdy(1, 1) = float(0);
    dRdy(1, 2) = tz;
    dRdy(2, 0) = mtw;
    dRdy(2, 1) = tz;
    dRdy(2, 2) = mfy;

    dRdz(0, 0) = mfz;
    dRdz(0, 1) = mtw;
    dRdz(0, 2) = tx;
    dRdz(1, 0) = tw;
    dRdz(1, 1) = mfz;
    dRdz(1, 2) = ty;
    dRdz(2, 0) = tx;
    dRdz(2, 1) = ty;
    dRdz(2, 2) = float(0);
  }

  // Derivative of Rotation Matrix with respect to quaternion
  // Used in Chain Rule
  __host__ __device__ __forceinline__ void dRtodq(const Eigen::Matrix3f dR,
                                                  float *grad,
                                                  const float der_p) const {
    grad[3] = der_p * (dR.transpose() * dRdw).trace();
    grad[0] = der_p * (dR.transpose() * dRdx).trace();
    grad[1] = der_p * (dR.transpose() * dRdy).trace();
    grad[2] = der_p * (dR.transpose() * dRdz).trace();
  }

  __host__ __device__ CostStruct operator()(const NearestNStruct &u) {
    CostStruct out;

    Eigen::Matrix3f temp = u.target_covariance_ + u.transformed_covariance_;
    Eigen::Matrix3f M = Inverse(temp);

    Eigen::Vector3f res =
        Eigen::Map<const Eigen::Vector3f>(u.target_point_.data) -
        Eigen::Map<const Eigen::Vector3f>(u.transformed_point_.data);

    Eigen::Vector3f dT = M * res;

    const float sum = 1.f + (res.transpose() * dT)(0, 0) * c;
    const float inv = 1.f / sum;
    out.cost = b * log(sum);

    if (isnan(sum)) {
      printf("%f\n %f\n %f\n", res(0, 0), res(1, 0), res(2, 0));
      printf("%f %f %f\n%f %f %f\n%f %f %f\n\n", M(0, 0), M(0, 1), M(0, 2),
             M(1, 0), M(1, 1), M(1, 2), M(2, 0), M(2, 1), M(2, 2));
    }

    if (compute_jacobian) {
      const float der_p = max(1e-6, inv);
      Eigen::Matrix3f Ta =
          Inverse(u.target_covariance_.transpose() + u.transformed_covariance_);
      Eigen::Vector3f tb = M * res;
      Eigen::Vector3f tc = Ta * res;
      Eigen::Matrix3f dR =
          -(tb * Eigen::Map<const Eigen::Vector3f>(u.source_point_.data)
                     .transpose() +
            tc * (res.transpose() * Ta * R * u.source_covariance_.transpose()) +
            tb * (res.transpose() * M * R * u.source_covariance_) +
            tc * Eigen::Map<const Eigen::Vector3f>(u.source_point_.data)
                     .transpose());
      dT *= -2.0;
      dRtodq(dR, out.gradient, der_p);
      out.gradient[4] = der_p * dT(0);
      out.gradient[5] = der_p * dT(1);
      out.gradient[6] = der_p * dT(2);
    }
    return out;
  }
};

struct SE3Transform {
  Eigen::Matrix3f rotation_;
  Eigen::Vector3f translation_;

  __host__ __device__ SE3Transform() {}

  __host__ __device__ SE3Transform(const Sophus::SE3f t) {
    translation_ = t.translation();
    rotation_ = t.rotationMatrix();
  }

  __host__ __device__ __forceinline__ cuPointXYZ
  operator()(const cuPointXYZ &in) {
    cuPointXYZ out;
    Eigen::Map<Eigen::Vector3f>(out.data) =
        rotation_ * Eigen::Map<const Eigen::Vector3f>(in.data) + translation_;
    return out;
  }

  __host__ __device__ __forceinline__ Eigen::Matrix3f operator()(
      const Eigen::Matrix3f &in) {
    return rotation_ * in * rotation_.transpose();
  }

  __host__ __device__ __forceinline__ NearestNStruct
  operator()(const NearestNStruct &in) {
    NearestNStruct out;
    out.source_point_ = in.source_point_;
    out.source_covariance_ = in.source_covariance_;
    out.target_point_ = in.target_point_;
    out.target_covariance_ = in.target_covariance_;
    Eigen::Map<Eigen::Vector3f>(out.transformed_point_.data) =
        rotation_ * Eigen::Map<const Eigen::Vector3f>(in.source_point_.data) +
        translation_;
    out.transformed_covariance_ =
        rotation_ * in.source_covariance_ * rotation_.transpose();
    return out;
  }
};

__global__ void ComputeCovarianceVector(
    KernelArray<cuPointXYZ> points, KernelArray<int> k_nns,
    KernelArray<Eigen::Matrix3f> covariances, int k) {
  int pos = blockIdx.x * blockDim.x + threadIdx.x;
  if (pos >= points.size) return;

  Eigen::Vector3f mean(0, 0, 0);
  Eigen::Matrix3f covariance = Eigen::Matrix3f::Zero();

  for (int i = pos * k; i < (pos + 1) * k; i++) {
    mean = mean + points.data[k_nns.data[i]].toVec();
  }
  mean = mean * (1.0f / (float)(k));

  for (int i = pos * k; i < (pos + 1) * k; i++) {
    Eigen::Vector3f temp = points.data[k_nns.data[i]].toVec() - mean;
    Eigen::Matrix3f temp_m = temp * temp.transpose();
    covariance = covariance + temp_m;
  }

  /* PCA */
  Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> es(3);
  es.computeDirect(covariance);
  // Eigen values are sorted
  Eigen::Matrix3f eigen_value_replacement = Eigen::Matrix3f::Zero();
  eigen_value_replacement(0, 0) = 1e-3;
  eigen_value_replacement(1, 1) = 1.0;
  eigen_value_replacement(2, 2) = 1.0;
  covariances.data[pos] = es.eigenvectors() * eigen_value_replacement *
                          es.eigenvectors().transpose();
  covariance = covariances.data[pos];

  return;
}

__global__ void PopulateNearestNStruct(
    KernelArray<cuPointXYZ> source_points,
    KernelArray<Eigen::Matrix3f> source_covariances,
    KernelArray<cuPointXYZ> target_points,
    KernelArray<Eigen::Matrix3f> target_covariances,
    KernelArray<cuPointXYZ> transformed_points,
    KernelArray<Eigen::Matrix3f> transformed_covariances,
    KernelArray<int> indices, KernelArray<NearestNStruct> nn_vector) {
  int pos = blockIdx.x * blockDim.x + threadIdx.x;
  if (pos >= source_points.size) return;

  nn_vector.data[pos].source_point_ = source_points.data[pos];
  nn_vector.data[pos].source_covariance_ = source_covariances.data[pos];
  nn_vector.data[pos].transformed_point_ = transformed_points.data[pos];
  nn_vector.data[pos].transformed_covariance_ =
      transformed_covariances.data[pos];
  nn_vector.data[pos].target_point_ = target_points.data[indices.data[pos]];
  nn_vector.data[pos].target_covariance_ =
      target_covariances.data[indices.data[pos]];
}

bool cuGICP::Align(const Sophus::SE3f initial_transformation,
                   cuPointCloud<PointType>::SharedPtr final_source) {
  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  cudaEventRecord(start);
  if (d_source_kd_tree_ == nullptr) {
    ComputeSourceCovariances();
  }

  if (d_target_kd_tree_ == nullptr) {
    ComputeTargetCovariances();
  }
  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  float temp_milliseconds;
  cudaEventElapsedTime(&temp_milliseconds, start, stop);
  std::cout << "Cov Elapsed time: " << temp_milliseconds << std::endl;

  Sophus::SE3f current_transformation(initial_transformation);
  Sophus::SE3f last_transformation(initial_transformation);

  auto transformed_source_points = std::make_shared<cuPointCloud<PointType>>();
  transformed_source_points->points.resize(d_source_cloud_->size());
  auto transformed_source_covariances =
      std::make_shared<CovarianceVector>(d_source_cloud_->size());

  auto nearest_n_indices =
      std::make_shared<thrust::device_vector<int>>(d_source_cloud_->size());

  auto nearest_n_vector =
      std::make_shared<thrust::device_vector<NearestNStruct>>(
          d_source_cloud_->size());
  // std::cout << "Allocated nn struct\n";

  float param_change = std::numeric_limits<float>::max();
  size_t iterations = 0;
  bool converged = false;
  bool hit_max_iterations = false;
  while (converged == false) {
    cudaEventRecord(start);
    SE3Transform trans_struct(current_transformation);
    thrust::transform(d_source_cloud_->begin(), d_source_cloud_->end(),
                      transformed_source_points->begin(), trans_struct);
    SE3Transform trans_struct2(current_transformation);
    thrust::transform(d_source_covariances_->begin(),
                      d_source_covariances_->end(),
                      transformed_source_covariances->begin(), trans_struct2);

    d_target_kd_tree_->NearestKSearch(transformed_source_points, 1,
                                      *nearest_n_indices);

    int blocks = ceil((float)d_source_cloud_->size() / THREADNUM);
    PopulateNearestNStruct<<<blocks, THREADNUM>>>(
        d_source_cloud_->points, *d_source_covariances_,
        d_target_cloud_->points, *d_target_covariances_,
        transformed_source_points->points, *transformed_source_covariances,
        *nearest_n_indices, *nearest_n_vector);
    // std::cout << "Populated nn struct\n";
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    float milliseconds;
    cudaEventElapsedTime(&milliseconds, start, stop);
    std::cout << "NN create Elapsed time: " << milliseconds << std::endl;

    ceres::GradientProblem problem(new GICPProblem(nearest_n_vector, alpha_),
                                   new LocalParameterizationSE3());
    // std::cout << "Created Ceres problem\n";

    ceres::GradientProblemSolver::Options options;
    options.function_tolerance = 1e-8;
    options.gradient_tolerance = 1e-8;
    options.parameter_tolerance = 1e-8;
    options.line_search_direction_type = ceres::BFGS;
    ceres::GradientProblemSolver::Summary summary;

    Sophus::SE3d double_temp = current_transformation.cast<double>();
    ceres::Solve(options, problem, double_temp.data(), &summary);
    std::cout << "Solved Ceres problem\n";
    std::cout << summary.FullReport() << std::endl;
    current_transformation = double_temp.cast<float>();

    iterations++;
    param_change = (last_transformation.inverse() * current_transformation)
                       .log()
                       .squaredNorm();
    if (param_change < epsilon_ || iterations > max_itertations_) {
      converged = true;
      if (iterations > max_itertations_) {
        hit_max_iterations = true;
      }
    }

    last_transformation = current_transformation;
  }

  final_transformation_ = current_transformation;
  std::cout << "All done\n";
  std::cout << final_transformation_.matrix() << std::endl;

  if (final_source != nullptr) {
    SE3Transform trans_struct(final_transformation_);
    thrust::transform(d_source_cloud_->begin(), d_source_cloud_->end(),
                      final_source->begin(), trans_struct);
  }

  return hit_max_iterations;
}

void cuGICP::ComputeSourceCovariances() {
  // std::cout << "computing source covs\n";
  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  cudaEventRecord(start);
  d_source_kd_tree_ = std::make_shared<cuKdTree<cuPointXYZ>>();
  d_source_kd_tree_->SetInputCloud(d_source_cloud_);
  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  float milliseconds;
  cudaEventElapsedTime(&milliseconds, start, stop);
  std::cout << "Kd Tree Elapsed time: " << milliseconds << std::endl;

  cudaEventRecord(start);
  thrust::device_vector<int> indices(d_source_cloud_->size() *
                                     k_correspondences_);
  d_source_kd_tree_->NearestKSearch(d_source_cloud_, k_correspondences_,
                                    indices);

  d_source_covariances_ =
      std::make_shared<CovarianceVector>(d_source_cloud_->size());

  int blocks = ceil((float)d_source_cloud_->size() / THREADNUM);
  ComputeCovarianceVector<<<blocks, THREADNUM>>>(
      d_source_cloud_->points, indices, *d_source_covariances_,
      k_correspondences_);
  cudaSafe(cudaDeviceSynchronize());
  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  cudaEventElapsedTime(&milliseconds, start, stop);
  std::cout << "Covariance Elapsed time: " << milliseconds << std::endl;
}

void cuGICP::ComputeTargetCovariances() {
  // std::cout << "computing target covs\n";
  d_target_kd_tree_ = std::make_shared<cuKdTree<cuPointXYZ>>();
  d_target_kd_tree_->SetInputCloud(d_target_cloud_);
  thrust::device_vector<int> indices(d_target_cloud_->size() *
                                     k_correspondences_);
  d_target_kd_tree_->NearestKSearch(d_target_cloud_, k_correspondences_,
                                    indices);

  d_target_covariances_ =
      std::make_shared<CovarianceVector>(d_target_cloud_->size());

  int blocks = ceil((float)d_target_cloud_->size() / THREADNUM);
  ComputeCovarianceVector<<<blocks, THREADNUM>>>(
      d_target_cloud_->points, indices, *d_target_covariances_,
      k_correspondences_);
  cudaSafe(cudaDeviceSynchronize());
}

bool GICPProblem::Evaluate(const double *parameters, double *cost,
                           double *gradient) const {
  float parametersf[7];
  std::copy(parameters, parameters + 7, parametersf);
  Eigen::Map<Sophus::SE3f const> const transformation(parametersf);
  SE3Transform trans_struct(transformation);
  thrust::transform(nearest_n_vector_->begin(), nearest_n_vector_->end(),
                    nearest_n_vector_->begin(), trans_struct);
  // std::cout << "Transformed NNstruct\n";

  bool compute_jacobian = true;
  if (gradient == NULL) compute_jacobian = false;

  GICPCostFunctor gicp_functor(alpha_, compute_jacobian,
                               transformation.unit_quaternion());
  CostStruct cs;
  cs.cost = 0;
  std::fill(cs.gradient, cs.gradient + 7, 0);

  // std::cout << "Pre Cost\n";
  CostStruct cost_struct = thrust::transform_reduce(
      nearest_n_vector_->begin(), nearest_n_vector_->end(), gicp_functor, cs,
      thrust::plus<CostStruct>());
  // thrust::device_vector<CostStruct> cost_vec(nearest_n_vector_->size());
  // thrust::transform(nearest_n_vector_->begin(), nearest_n_vector_->end(),
  //    cost_vec.begin(), gicp_functor);
  // std::cout << "Transformed to cost\n";
  // std::cout << "Computed Cost\n";

  cost[0] = cost_struct.cost;

  if (compute_jacobian)
    std::copy(cost_struct.gradient, cost_struct.gradient + 7, gradient);

  return true;
}

}  // namespace perl_registration
