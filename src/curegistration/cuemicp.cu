#include "cuemicp.h"

#include <Eigen/Eigenvalues>

#include <thrust/device_vector.h>
#include <thrust/functional.h>
#include <thrust/transform.h>

#include "common/gpuutil.h"
#include "common/inverse.h"
#include "common/local_parameterization_se3.h"

#define THREADNUM 128

namespace perl_registration {

template <size_t N>
__host__ __device__ typename cuEMICP<N>::CostStruct operator+(
    const typename cuEMICP<N>::CostStruct &a,
    const typename cuEMICP<N>::CostStruct &b) {
  typename cuEMICP<N>::CostStruct out;
  out.cost = a.cost + b.cost;
  out.gradient[0] = a.gradient[0] + b.gradient[0];
  out.gradient[1] = a.gradient[1] + b.gradient[1];
  out.gradient[2] = a.gradient[2] + b.gradient[2];
  out.gradient[3] = a.gradient[3] + b.gradient[3];
  out.gradient[4] = a.gradient[4] + b.gradient[4];
  out.gradient[5] = a.gradient[5] + b.gradient[5];
  out.gradient[6] = a.gradient[6] + b.gradient[6];
  return out;
};

template <size_t N>
__host__ __device__ cuEMICP<N>::EMICPCostFunctor::EMICPCostFunctor(
    const float &alpha_, const bool &compute_jacobian_,
    const Eigen::Quaternionf &q)
    : compute_jacobian(compute_jacobian_), b(alpha_ * alpha_), c(1.f / b) {
  R = q.toRotationMatrix();
  const float tx = float(2) * q.x();
  const float ty = float(2) * q.y();
  const float tz = float(2) * q.z();
  const float tw = float(2) * q.w();
  const float mfx = float(-2) * tx;
  const float mfy = float(-2) * ty;
  const float mfz = float(-2) * tz;
  const float mtw = float(-1) * tw;
  /* Eigen Quat to Rot
  res.coeffRef(0,0) = Scalar(1)-(tyy+tzz);
  res.coeffRef(0,1) = txy-twz;
  res.coeffRef(0,2) = txz+twy;
  res.coeffRef(1,0) = txy+twz;
  res.coeffRef(1,1) = Scalar(1)-(txx+tzz);
  res.coeffRef(1,2) = tyz-twx;
  res.coeffRef(2,0) = txz-twy;
  res.coeffRef(2,1) = tyz+twx;
  res.coeffRef(2,2) = Scalar(1)-(txx+tyy);
  */
  dRdw(0, 0) = float(0);
  dRdw(0, 1) = -tz;
  dRdw(0, 2) = ty;
  dRdw(1, 0) = tz;
  dRdw(1, 1) = float(0);
  dRdw(1, 2) = -tx;
  dRdw(2, 0) = -ty;
  dRdw(2, 1) = tx;
  dRdw(2, 2) = float(0);

  dRdx(0, 0) = float(0);
  dRdx(0, 1) = ty;
  dRdx(0, 2) = tz;
  dRdx(1, 0) = ty;
  dRdx(1, 1) = mfx;
  dRdx(1, 2) = mtw;
  dRdx(2, 0) = tz;
  dRdx(2, 1) = tw;
  dRdx(2, 2) = mfx;

  dRdy(0, 0) = mfy;
  dRdy(0, 1) = tx;
  dRdy(0, 2) = tw;
  dRdy(1, 0) = tx;
  dRdy(1, 1) = float(0);
  dRdy(1, 2) = tz;
  dRdy(2, 0) = mtw;
  dRdy(2, 1) = tz;
  dRdy(2, 2) = mfy;

  dRdz(0, 0) = mfz;
  dRdz(0, 1) = mtw;
  dRdz(0, 2) = tx;
  dRdz(1, 0) = tw;
  dRdz(1, 1) = mfz;
  dRdz(1, 2) = ty;
  dRdz(2, 0) = tx;
  dRdz(2, 1) = ty;
  dRdz(2, 2) = float(0);
}

template <size_t N>
__host__ __device__ typename cuEMICP<N>::CostStruct
cuEMICP<N>::EMICPCostFunctor::operator()(const NearestNStruct &u) {
  CostStruct out;

  Eigen::Matrix3f temp = u.target_covariance_ + u.transformed_covariance_;
  Eigen::Matrix3f M = Inverse(temp);

  Eigen::Vector3f res =
      Eigen::Map<const Eigen::Vector3f>(u.target_point_.data) -
      Eigen::Map<const Eigen::Vector3f>(u.transformed_point_.data);

  Eigen::Vector3f dT = M * res;

  const float sum =
      1.f + (res.transpose() * dT)(0, 0) * u.semantic_probability * c;
  const float inv = 1.f / sum;
  out.cost = b * log(sum);

  if (compute_jacobian) {
    const float der_p = max(1e-6, inv);
    Eigen::Matrix3f Ta =
        Inverse(u.target_covariance_.transpose() + u.transformed_covariance_);
    Eigen::Vector3f tb = M * res;
    Eigen::Vector3f tc = Ta * res;
    Eigen::Matrix3f dR =
        -(tb * Eigen::Map<const Eigen::Vector3f>(u.source_point_.data)
                   .transpose() +
          tc * (res.transpose() * Ta * R * u.source_covariance_.transpose()) +
          tb * (res.transpose() * M * R * u.source_covariance_) +
          tc * Eigen::Map<const Eigen::Vector3f>(u.source_point_.data)
                   .transpose());
    dT *= -2.0 * u.semantic_probability * der_p;
    dRtodq(dR, out.gradient, der_p * u.semantic_probability);
    out.gradient[4] = dT(0);
    out.gradient[5] = dT(1);
    out.gradient[6] = dT(2);
  }
  return out;
};

template <size_t N>
__global__ void ComputeCovarianceVector(
    KernelArray<cuPointXYZP<N>> points, KernelArray<int> k_nns,
    KernelArray<Eigen::Matrix3f> covariances, int k) {
  int pos = blockIdx.x * blockDim.x + threadIdx.x;
  if (pos >= points.size) return;

  Eigen::Vector3f mean(0, 0, 0);
  Eigen::Matrix3f covariance = Eigen::Matrix3f::Zero();

  for (int i = pos * k; i < (pos + 1) * k; i++) {
    mean = mean + points.data[k_nns.data[i]].toVec();
  }
  mean = mean * (1.0f / (float)(k));

  for (int i = pos * k; i < (pos + 1) * k; i++) {
    Eigen::Vector3f temp = points.data[k_nns.data[i]].toVec() - mean;
    Eigen::Matrix3f temp_m = temp * temp.transpose();
    covariance = covariance + temp_m;
  }

  /* PCA */
  Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> es(3);
  es.computeDirect(covariance);
  // Eigen values are sorted
  Eigen::Matrix3f eigen_value_replacement = Eigen::Matrix3f::Zero();
  eigen_value_replacement(0, 0) = 1e-4;
  eigen_value_replacement(1, 1) = 1;
  eigen_value_replacement(2, 2) = 1;
  covariances.data[pos] = es.eigenvectors() * eigen_value_replacement *
                          es.eigenvectors().transpose();
  covariance = covariances.data[pos];

  return;
}

template <size_t N>
__global__ void PopulateNearestNStruct(
    KernelArray<cuPointXYZP<N>> source_points,
    KernelArray<Eigen::Matrix3f> source_covariances,
    KernelArray<cuPointXYZP<N>> target_points,
    KernelArray<Eigen::Matrix3f> target_covariances,
    KernelArray<cuPointXYZP<N>> transformed_points,
    KernelArray<Eigen::Matrix3f> transformed_covariances,
    KernelArray<int> indices,
    KernelArray<typename cuEMICP<N>::NearestNStruct> nn_vector, size_t k) {
  int pos = blockIdx.x * blockDim.x + threadIdx.x;
  if (pos >= source_points.size) return;

  for (int i = pos * k; i < (pos + 1) * k; i++) {
    nn_vector.data[i].source_point_ = source_points.data[i];
    nn_vector.data[i].source_covariance_ = source_covariances.data[i];
    nn_vector.data[i].transformed_point_ = transformed_points.data[i];
    nn_vector.data[i].transformed_covariance_ = transformed_covariances.data[i];
    nn_vector.data[i].target_point_ = target_points.data[indices.data[i]];
    nn_vector.data[i].target_covariance_ =
        target_covariances.data[indices.data[i]];
    nn_vector.data[i].semantic_probability =
        source_points.data[i].cuConstProbabilityMap().transpose() *
        target_points.data[indices.data[i]].cuConstProbabilityMap();
  }
}

template <size_t N>
bool cuEMICP<N>::Align(
    const Sophus::SE3f initial_transformation,
    typename cuPointCloud<PointType>::SharedPtr final_source) {
  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  cudaEventRecord(start);
  if (d_source_kd_tree_ == nullptr) {
    ComputeSourceCovariances();
  }

  if (d_target_kd_tree_ == nullptr) {
    ComputeTargetCovariances();
  }
  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  float temp_milliseconds;
  cudaEventElapsedTime(&temp_milliseconds, start, stop);
  std::cout << "Cov Elapsed time: " << temp_milliseconds << std::endl;

  Sophus::SE3f current_transformation(initial_transformation);
  Sophus::SE3f last_transformation(initial_transformation);

  auto transformed_source_points = std::make_shared<cuPointCloud<PointType>>();
  transformed_source_points->points.resize(d_source_cloud_->size());
  auto transformed_source_covariances =
      std::make_shared<CovarianceVector>(d_source_cloud_->size());

  auto nearest_n_indices = std::make_shared<thrust::device_vector<int>>(
      d_source_cloud_->size() * em_neighbors_);

  auto nearest_n_vector =
      std::make_shared<thrust::device_vector<NearestNStruct>>(
          d_source_cloud_->size() * em_neighbors_);
  // std::cout << "Allocated nn struct\n";

  float param_change = std::numeric_limits<float>::max();
  size_t iterations = 0;
  bool converged = false;
  bool hit_max_iterations = false;
  while (converged == false) {
    cudaEventRecord(start);
    SE3Transform trans_struct(current_transformation);
    thrust::transform(d_source_cloud_->begin(), d_source_cloud_->end(),
                      transformed_source_points->begin(), trans_struct);
    SE3Transform trans_struct2(current_transformation);
    thrust::transform(d_source_covariances_->begin(),
                      d_source_covariances_->end(),
                      transformed_source_covariances->begin(), trans_struct2);

    d_target_kd_tree_->NearestKSearch(transformed_source_points, em_neighbors_,
                                      *nearest_n_indices);

    int blocks = ceil((float)d_source_cloud_->size() / THREADNUM);
    PopulateNearestNStruct<N><<<blocks, THREADNUM>>>(
        d_source_cloud_->points, *d_source_covariances_,
        d_target_cloud_->points, *d_target_covariances_,
        transformed_source_points->points, *transformed_source_covariances,
        *nearest_n_indices, *nearest_n_vector);
    // std::cout << "Populated nn struct\n";
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    float milliseconds;
    cudaEventElapsedTime(&temp_milliseconds, start, stop);
    std::cout << "NN create Elapsed time: " << milliseconds << std::endl;

    ceres::GradientProblem problem(new EMICPProblem(nearest_n_vector, alpha_),
                                   new LocalParameterizationSE3());
    // std::cout << "Created Ceres problem\n";

    ceres::GradientProblemSolver::Options options;
    options.function_tolerance = 1e-5;
    options.gradient_tolerance = 1e-5;
    options.parameter_tolerance = 1e-5;
    options.line_search_direction_type = ceres::BFGS;
    ceres::GradientProblemSolver::Summary summary;

    Sophus::SE3d double_temp = current_transformation.cast<double>();
    ceres::Solve(options, problem, double_temp.data(), &summary);
    std::cout << "Solved Ceres problem\n";
    std::cout << summary.FullReport() << std::endl;
    current_transformation = double_temp.cast<float>();

    iterations++;
    param_change = (last_transformation.inverse() * current_transformation)
                       .log()
                       .squaredNorm();
    if (param_change < epsilon_ || iterations > max_itertations_) {
      converged = true;
      if (iterations > max_itertations_) {
        hit_max_iterations = true;
      }
    }

    last_transformation = current_transformation;
  }

  final_transformation_ = current_transformation;
  std::cout << "All done\n";
  std::cout << final_transformation_.matrix() << std::endl;

  if (final_source != nullptr) {
    SE3Transform trans_struct(final_transformation_);
    thrust::transform(d_source_cloud_->begin(), d_source_cloud_->end(),
                      final_source->begin(), trans_struct);
  }

  return hit_max_iterations;
}

template <size_t N>
void cuEMICP<N>::ComputeSourceCovariances() {
  // std::cout << "computing source covs\n";
  d_source_kd_tree_ = std::make_shared<cuKdTree<cuPointXYZP<N>>>();
  d_source_kd_tree_->SetInputCloud(d_source_cloud_);
  thrust::device_vector<int> indices(d_source_cloud_->size() *
                                     k_correspondences_);
  d_source_kd_tree_->NearestKSearch(d_source_cloud_, k_correspondences_,
                                    indices);

  d_source_covariances_ =
      std::make_shared<CovarianceVector>(d_source_cloud_->size());

  int blocks = ceil((float)d_source_cloud_->size() / THREADNUM);
  ComputeCovarianceVector<N>
      <<<blocks, THREADNUM>>>(d_source_cloud_->points, indices,
                              *d_source_covariances_, k_correspondences_);
  cudaSafe(cudaDeviceSynchronize());
}

template <size_t N>
void cuEMICP<N>::ComputeTargetCovariances() {
  // std::cout << "computing target covs\n";
  d_target_kd_tree_ = std::make_shared<cuKdTree<cuPointXYZP<N>>>();
  d_target_kd_tree_->SetInputCloud(d_target_cloud_);
  thrust::device_vector<int> indices(d_target_cloud_->size() *
                                     k_correspondences_);
  d_target_kd_tree_->NearestKSearch(d_target_cloud_, k_correspondences_,
                                    indices);

  d_target_covariances_ =
      std::make_shared<CovarianceVector>(d_target_cloud_->size());

  int blocks = ceil((float)d_target_cloud_->size() / THREADNUM);
  ComputeCovarianceVector<N>
      <<<blocks, THREADNUM>>>(d_target_cloud_->points, indices,
                              *d_target_covariances_, k_correspondences_);
  cudaSafe(cudaDeviceSynchronize());
}

template <size_t N>
bool cuEMICP<N>::EMICPProblem::Evaluate(const double *parameters, double *cost,
                                        double *gradient) const {
  float parametersf[7];
  std::copy(parameters, parameters + 7, parametersf);
  Eigen::Map<Sophus::SE3f const> const transformation(parametersf);
  SE3Transform trans_struct(transformation);
  thrust::transform(nearest_n_vector_->begin(), nearest_n_vector_->end(),
                    nearest_n_vector_->begin(), trans_struct);
  // std::cout << "Transformed NNstruct\n";

  bool compute_jacobian = true;
  if (gradient == NULL) compute_jacobian = false;

  EMICPCostFunctor gicp_functor(alpha_, compute_jacobian,
                                transformation.unit_quaternion());
  CostStruct cs;
  cs.cost = 0;
  std::fill(cs.gradient, cs.gradient + 7, 0);

  // std::cout << "Pre Cost\n";
  CostStruct cost_struct = thrust::transform_reduce(
      nearest_n_vector_->begin(), nearest_n_vector_->end(), gicp_functor, cs,
      thrust::plus<CostStruct>());
  // thrust::device_vector<CostStruct> cost_vec(nearest_n_vector_->size());
  // thrust::transform(nearest_n_vector_->begin(), nearest_n_vector_->end(),
  //    cost_vec.begin(), gicp_functor);
  // std::cout << "Transformed to cost\n";
  // std::cout << "Computed Cost\n";

  cost[0] = cost_struct.cost;

  if (compute_jacobian)
    std::copy(cost_struct.gradient, cost_struct.gradient + 7, gradient);

  return true;
}

}  // namespace perl_registration
