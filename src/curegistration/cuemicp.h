#pragma once

#include <Eigen/Core>

#include <sophus/common.hpp>
#include <sophus/se3.hpp>
#include <sophus/types.hpp>

#include "ceres/ceres.h"

#include "cukdtree/cukdtree.h"
#include "cupointcloud/cupointcloud.h"
#include "cupointcloud/point_types.h"

namespace perl_registration {
template <size_t N>
class cuEMICP {
 public:
  typedef cuPointXYZP<N> PointType;
  typedef std::shared_ptr<cuEMICP> SharedPtr;
  typedef std::shared_ptr<const cuEMICP> SharedConstPtr;

  typedef Eigen::Matrix3f CovarianceType;
  typedef typename thrust::device_vector<CovarianceType> CovarianceVector;
  typedef typename std::shared_ptr<CovarianceVector> CovarianceVectorPtr;

  cuEMICP()
      : k_correspondences_(20),
        max_itertations_(50),
        epsilon_(1e-4),
        alpha_(20),
        em_neighbors_(8) {}

  inline void SetInputSource(
      const typename cuPointCloud<PointType>::SharedPtr &cloud) {
    d_source_cloud_ = cloud;
    d_source_kd_tree_ = nullptr;
    d_source_covariances_ = nullptr;
  }

  inline void SetInputTarget(
      const typename cuPointCloud<PointType>::SharedPtr &cloud) {
    d_target_cloud_ = cloud;
    d_target_kd_tree_ = nullptr;
    d_target_covariances_ = nullptr;
  }

  inline void SetInputSource(
      const typename cuPointCloud<PointType>::SharedPtr &cloud,
      const typename cuKdTree<PointType>::SharedPtr &kdtree,
      const CovarianceVectorPtr &covariances) {
    d_source_cloud_ = cloud;
    d_source_kd_tree_ = kdtree;
    d_source_covariances_ = covariances;
  }

  inline void SetInputTarget(
      const typename cuPointCloud<PointType>::SharedPtr &cloud,
      const typename cuKdTree<PointType>::SharedPtr &kdtree,
      const CovarianceVectorPtr &covariances) {
    d_target_cloud_ = cloud;
    d_target_kd_tree_ = kdtree;
    d_target_covariances_ = covariances;
  }

  inline void SetEpsilon(const float epsilon) { epsilon_ = epsilon; }

  inline float GetEpsilon() { return epsilon_; }

  inline void SetAlpha(const float alpha) { alpha_ = alpha; }

  inline float GetAlpha() { return alpha_; }

  inline void SetKCorrespondences(const size_t k) { k_correspondences_ = k; }

  inline size_t GetKCorrespondences() { return k_correspondences_; }

  inline void SetMaxIterations(const size_t max_itertations) {
    max_itertations_ = max_itertations;
  }

  inline size_t GetMaxIterations() { return max_itertations_; }

  inline Sophus::SE3f GetFinalTransformation() { return final_transformation_; }

  inline void SetEMNeighbors(const size_t k) { em_neighbors_ = k; }

  inline size_t GetEMNeighbors() { return em_neighbors_; }

  bool Align(const Sophus::SE3f initial_transformation,
             typename cuPointCloud<PointType>::SharedPtr final_source);

  struct NearestNStruct {
    cuPointXYZP<N> transformed_point_;
    Eigen::Matrix3f transformed_covariance_;
    cuPointXYZP<N> source_point_;
    Eigen::Matrix3f source_covariance_;
    cuPointXYZP<N> target_point_;
    Eigen::Matrix3f target_covariance_;
    float semantic_probability;
  };

 private:
  typename cuPointCloud<PointType>::SharedPtr d_source_cloud_;
  typename cuKdTree<PointType>::SharedPtr d_source_kd_tree_;
  CovarianceVectorPtr d_source_covariances_;

  typename cuPointCloud<PointType>::SharedPtr d_target_cloud_;
  typename cuKdTree<PointType>::SharedPtr d_target_kd_tree_;
  CovarianceVectorPtr d_target_covariances_;

  float epsilon_;
  float alpha_;
  size_t k_correspondences_;
  size_t em_neighbors_;
  size_t max_itertations_;
  Sophus::SE3f final_transformation_;

  void ComputeSourceCovariances();

  void ComputeTargetCovariances();

  struct CostStruct {
    float cost;
    float gradient[7];
  };

  struct EMICPCostFunctor {
    bool compute_jacobian;
    float b;
    float c;
    Eigen::Matrix3f R;
    Eigen::Matrix3f dRdw;
    Eigen::Matrix3f dRdx;
    Eigen::Matrix3f dRdy;
    Eigen::Matrix3f dRdz;

    __host__ __device__ EMICPCostFunctor(const float &, const bool &,
                                         const Eigen::Quaternionf &);

    // Derivative of Rotation Matrix with respect to quaternion
    // Used in Chain Rule
    __host__ __device__ __forceinline__ void dRtodq(const Eigen::Matrix3f &dR,
                                                    float *grad,
                                                    const float der_p) const {
      grad[3] = der_p * (dR.transpose() * dRdw).trace();
      grad[0] = der_p * (dR.transpose() * dRdx).trace();
      grad[1] = der_p * (dR.transpose() * dRdy).trace();
      grad[2] = der_p * (dR.transpose() * dRdz).trace();
    }

    __host__ __device__ CostStruct operator()(const NearestNStruct &);
  };

  struct SE3Transform {
    Eigen::Matrix3f rotation_;
    Eigen::Vector3f translation_;

    __host__ __device__ SE3Transform() {}

    __host__ __device__ SE3Transform(const Sophus::SE3f t) {
      translation_ = t.translation();
      rotation_ = t.rotationMatrix();
    }

    __host__ __device__ __forceinline__ cuPointXYZP<N> operator()(
        const cuPointXYZP<N> &in) {
      cuPointXYZP<N> out;
      Eigen::Map<Eigen::Vector3f>(out.data) =
          rotation_ * Eigen::Map<const Eigen::Vector3f>(in.data) + translation_;
      return out;
    }

    __host__ __device__ __forceinline__ Eigen::Matrix3f operator()(
        const Eigen::Matrix3f &in) {
      return rotation_ * in * rotation_.transpose();
    }

    __host__ __device__ __forceinline__ NearestNStruct
    operator()(const NearestNStruct &in) {
      NearestNStruct out;
      out.source_point_ = in.source_point_;
      out.source_covariance_ = in.source_covariance_;
      out.target_point_ = in.target_point_;
      out.target_covariance_ = in.target_covariance_;
      Eigen::Map<Eigen::Vector3f>(out.transformed_point_.data) =
          rotation_ * Eigen::Map<const Eigen::Vector3f>(in.source_point_.data) +
          translation_;
      out.transformed_covariance_ =
          rotation_ * in.source_covariance_ * rotation_.transpose();
      out.semantic_probability = in.semantic_probability;
      return out;
    }
  };

  class EMICPProblem : public ceres::FirstOrderFunction {
   public:
    EMICPProblem(const std::shared_ptr<thrust::device_vector<NearestNStruct>>
                     nearest_n_vector,
                 const float alpha)
        : alpha_(alpha) {
      nearest_n_vector_ = nearest_n_vector;
    }

    virtual ~EMICPProblem() {}

    virtual bool Evaluate(const double *parameters, double *cost,
                          double *gradient) const;

    virtual int NumParameters() const { return 7; }

   private:
    std::shared_ptr<thrust::device_vector<NearestNStruct>> nearest_n_vector_;
    const float alpha_;
  };
};

}  // namespace perl_registration
