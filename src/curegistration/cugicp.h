#pragma once

#include <Eigen/Core>

#include <sophus/common.hpp>
#include <sophus/se3.hpp>
#include <sophus/types.hpp>

#include "ceres/ceres.h"

#include "cukdtree/cukdtree.h"
#include "cupointcloud/cupointcloud.h"

namespace perl_registration {

struct NearestNStruct {
  cuPointXYZ transformed_point_;
  Eigen::Matrix3f transformed_covariance_;
  cuPointXYZ source_point_;
  Eigen::Matrix3f source_covariance_;
  cuPointXYZ target_point_;
  Eigen::Matrix3f target_covariance_;
};

class cuGICP {
 public:
  typedef cuPointXYZ PointType;
  typedef std::shared_ptr<cuGICP> SharedPtr;
  typedef std::shared_ptr<const cuGICP> SharedConstPtr;

  typedef Eigen::Matrix3f CovarianceType;
  typedef typename thrust::device_vector<CovarianceType> CovarianceVector;
  typedef typename std::shared_ptr<CovarianceVector> CovarianceVectorPtr;

  cuGICP()
      : k_correspondences_(12),
        max_itertations_(50),
        epsilon_(1e-4),
        alpha_(5) {}

  inline void SetInputSource(const cuPointCloud<PointType>::SharedPtr &cloud) {
    d_source_cloud_ = cloud;
    d_source_kd_tree_ = nullptr;
    d_source_covariances_ = nullptr;
  }

  inline void SetInputTarget(const cuPointCloud<PointType>::SharedPtr &cloud) {
    d_target_cloud_ = cloud;
    d_target_kd_tree_ = nullptr;
    d_target_covariances_ = nullptr;
  }

  inline void SetInputSource(const cuPointCloud<PointType>::SharedPtr &cloud,
                             const cuKdTree<PointType>::SharedPtr &kdtree,
                             const CovarianceVectorPtr &covariances) {
    d_source_cloud_ = cloud;
    d_source_kd_tree_ = kdtree;
    d_source_covariances_ = covariances;
  }

  inline void GetSourceVectors(cuKdTree<PointType>::SharedPtr &kdtree,
                               CovarianceVectorPtr &covariances) {
    kdtree = d_source_kd_tree_;
    covariances = d_source_covariances_;
  }

  inline void SetInputTarget(const cuPointCloud<PointType>::SharedPtr &cloud,
                             const cuKdTree<PointType>::SharedPtr &kdtree,
                             const CovarianceVectorPtr &covariances) {
    d_target_cloud_ = cloud;
    d_target_kd_tree_ = kdtree;
    d_target_covariances_ = covariances;
  }

  inline void GetTargetVectors(cuKdTree<PointType>::SharedPtr &kdtree,
                               CovarianceVectorPtr &covariances) {
    kdtree = d_target_kd_tree_;
    covariances = d_target_covariances_;
  }

  inline void SetEpsilon(const float epsilon) { epsilon_ = epsilon; }

  inline float GetEpsilon() { return epsilon_; }

  inline void SetAlpha(const float alpha) { alpha_ = alpha; }

  inline float GetAlpha() { return alpha_; }

  inline void SetKCorrespondences(const size_t k) { k_correspondences_ = k; }

  inline size_t GetKCorrespondences() { return k_correspondences_; }

  inline void SetMaxIterations(const size_t max_itertations) {
    max_itertations_ = max_itertations;
  }

  inline size_t GetMaxIterations() { return max_itertations_; }

  inline Sophus::SE3f GetFinalTransformation() { return final_transformation_; }

  bool Align(const Sophus::SE3f initial_transformation,
             cuPointCloud<PointType>::SharedPtr final_source);

 private:
  cuPointCloud<PointType>::SharedPtr d_source_cloud_;
  cuKdTree<PointType>::SharedPtr d_source_kd_tree_;
  CovarianceVectorPtr d_source_covariances_;

  cuPointCloud<PointType>::SharedPtr d_target_cloud_;
  cuKdTree<PointType>::SharedPtr d_target_kd_tree_;
  CovarianceVectorPtr d_target_covariances_;

  float epsilon_;
  float alpha_;
  size_t k_correspondences_;
  size_t max_itertations_;
  Sophus::SE3f final_transformation_;

  void ComputeSourceCovariances();

  void ComputeTargetCovariances();
};

class GICPProblem : public ceres::FirstOrderFunction {
 public:
  GICPProblem(const std::shared_ptr<thrust::device_vector<NearestNStruct>>
                  nearest_n_vector,
              const float alpha)
      : alpha_(alpha) {
    nearest_n_vector_ = nearest_n_vector;
  }

  virtual ~GICPProblem() {}

  virtual bool Evaluate(const double *parameters, double *cost,
                        double *gradient) const;

  virtual int NumParameters() const { return 7; }

 private:
  std::shared_ptr<thrust::device_vector<NearestNStruct>> nearest_n_vector_;
  const float alpha_;
};

}  // namespace perl_registration
