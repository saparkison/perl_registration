
#define THREADNUM 128

#define CUDA_STACK 50
#include "cupointcloud.h"

#define cudaSafe(ans) \
  { gpuAssert((ans), __FILE__, __LINE__, true); }

namespace perl_registration {}
