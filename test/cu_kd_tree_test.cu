#include <iostream>
#include <memory>
#include <random>

#include "gtest/gtest.h"

#include <pcl/kdtree/kdtree_flann.h>
#include <thrust/device_vector.h>
#include "pcl/point_cloud.h"

#include "cukdtree/cukdtree.h"
#include "cupointcloud/cupointcloud.h"

// Test kd tree look up
TEST(cuKdTreeTest, MatchesPCLKdTree) {
  std::uniform_real_distribution<> d(-3, 3);
  std::random_device rd;
  std::mt19937 gen(rd());
  pcl::PointCloud<pcl::PointXYZ> pc;

  for (size_t i = 0; i < 1000; i++) {
    pcl::PointXYZ p(d(gen), d(gen), d(gen));
    pc.push_back(p);
  }

  auto d_pc = std::make_shared<
      perl_registration::cuPointCloud<perl_registration::cuPointXYZ>>(pc);

  perl_registration::cuKdTree<perl_registration::cuPointXYZ> kd_tree;
  kd_tree.SetInputCloud(d_pc);

  pcl::PointCloud<pcl::PointXYZ> query;
  pcl::PointXYZ q(d(gen), d(gen), d(gen));
  query.push_back(q);
  auto d_query = std::make_shared<
      perl_registration::cuPointCloud<perl_registration::cuPointXYZ>>(query);

  thrust::device_vector<int> results;
  kd_tree.NearestKSearch(d_query, 1, results);

  int r = results[0];
  perl_registration::cuPointXYZ p = d_pc->points[r];

  std::cout << "cuKdTREE\n";
  std::cout << "Nearest Neighbor is Index " << r
            << " and has cordinates x: " << p.x << " y: " << p.y
            << " z: " << p.z << std::endl;

  pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;

  kdtree.setInputCloud(pc.makeShared());

  // K nearest neighbor search
  int K = 1;

  std::vector<int> pointIdxNKNSearch(K);
  std::vector<float> pointNKNSquaredDistance(K);

  if (kdtree.nearestKSearch(q, K, pointIdxNKNSearch, pointNKNSquaredDistance) >
      0) {
    std::cout << "pcl::KdTREE\n";
    for (size_t i = 0; i < pointIdxNKNSearch.size(); ++i) {
      std::cout << " Nearest Neighbor is Index " << pointIdxNKNSearch[i]
                << " and has cordinates x: "
                << pc.points[pointIdxNKNSearch[i]].x
                << " y: " << pc.points[pointIdxNKNSearch[i]].y
                << " x: " << pc.points[pointIdxNKNSearch[i]].z << std::endl;
    }
  }
  ASSERT_FLOAT_EQ(p.x, pc.points[pointIdxNKNSearch[0]].x);
  ASSERT_FLOAT_EQ(p.y, pc.points[pointIdxNKNSearch[0]].y);
  ASSERT_FLOAT_EQ(p.z, pc.points[pointIdxNKNSearch[0]].z);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
