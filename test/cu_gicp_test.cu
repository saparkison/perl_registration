#include <iostream>
#include <memory>
#include <random>

#include "gtest/gtest.h"

#include <pcl/kdtree/kdtree_flann.h>
#include <thrust/device_vector.h>
#include <sophus/se3.hpp>
#include "pcl/point_cloud.h"

#include "cukdtree/cukdtree.h"
#include "cupointcloud/cupointcloud.h"
#include "curegistration/cugicp.h"
#include "kitti_devkit/kitti_util.h"

TEST(cuGICPTest, Converges) {
  std::string file1 = "../../data/000000.bin";
  std::string file2 = "../../data/000003.bin";

  pcl::PointCloud<pcl::PointXYZ> target_cloud = kitti2pcl(file1);
  auto cu_target_cloud = std::make_shared<
      perl_registration::cuPointCloud<perl_registration::cuPointXYZ>>(
      target_cloud);
  pcl::PointCloud<pcl::PointXYZ> source_cloud = kitti2pcl(file2);
  auto cu_source_cloud = std::make_shared<
      perl_registration::cuPointCloud<perl_registration::cuPointXYZ>>(
      source_cloud);

  perl_registration::cuGICP cu_gicp;
  cu_gicp.SetInputSource(cu_source_cloud);
  cu_gicp.SetInputTarget(cu_target_cloud);

  Sophus::SE3f init;
  ASSERT_TRUE(cu_gicp.Align(init, nullptr));
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
