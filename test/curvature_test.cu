#include <iostream>
#include <memory>
#include <random>

#include "gtest/gtest.h"

#include <pcl/kdtree/kdtree_flann.h>
#include <thrust/device_vector.h>
#include <sophus/se3.hpp>
#include "pcl/point_cloud.h"

#include "cukdtree/cukdtree.h"
#include "cupointcloud/cupointcloud.h"
#include "curvature/curvature.h"
#include "kitti_devkit/kitti_util.h"

TEST(cuGICPTest, Converges) {
  std::string file2 = "../../data/000003.bin";

  pcl::PointCloud<pcl::PointXYZ> source_cloud = kitti2pcl(file2);
  auto cu_source_cloud = std::make_shared<
      perl_registration::cuPointCloud<perl_registration::cuPointXYZ>>(
      source_cloud);
  auto cu_curvature = std::make_shared<
      perl_registration::cuPointCloud<perl_registration::cuPointXYZI>>();

  perl_registration::Curvature curvature;
  curvature.SetInputSource(cu_source_cloud);
  curvature.Compute(cu_curvature);

  ASSERT_TRUE(true);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
