#include <iostream>
#include <memory>
#include <random>

#include <benchmark/benchmark.h>

#include <pcl/kdtree/kdtree_flann.h>
#include <thrust/device_vector.h>
#include <sophus/se3.hpp>
#include "pcl/point_cloud.h"

#include "cukdtree/cukdtree.h"
#include "cupointcloud/cupointcloud.h"
#include "curegistration/cugicp.h"
#include "kitti_devkit/kitti_util.h"

static void BM_cugicp(benchmark::State& state) {
  for (auto _ : state) {
    float millisseconds = 0;
    perl_registration::cuPointCloud<perl_registration::cuPointXYZ>::SharedPtr
        cu_target_point_cloud;
    perl_registration::cuKdTree<perl_registration::cuPointXYZ>::SharedPtr
        target_kd_tree;
    std::shared_ptr<thrust::device_vector<Eigen::Matrix3f>> target_covariances;

    Sophus::SE3f init;
    for (int i = 0; i < 9; i++) {
      char buff1[100];
      char buff2[100];
      snprintf(buff1, sizeof(buff1), "../../data/%06d.bin", i);
      snprintf(buff2, sizeof(buff2), "../../data/%06d.bin", i + 1);
      std::string file1 = buff1;
      std::string file2 = buff2;

      perl_registration::cuGICP cu_gicp;
      if (i == 0) {
        pcl::PointCloud<pcl::PointXYZ> target_cloud = kitti2pcl(file1);
        cu_target_point_cloud = std::make_shared<
            perl_registration::cuPointCloud<perl_registration::cuPointXYZ>>(
            target_cloud);
        cu_gicp.SetInputTarget(cu_target_point_cloud);
      } else {
        cu_gicp.SetInputTarget(cu_target_point_cloud, target_kd_tree,
                               target_covariances);
      }

      pcl::PointCloud<pcl::PointXYZ> source_cloud = kitti2pcl(file2);
      auto cu_source_cloud = std::make_shared<
          perl_registration::cuPointCloud<perl_registration::cuPointXYZ>>(
          source_cloud);
      cu_gicp.SetInputSource(cu_source_cloud);
      cu_gicp.SetAlpha(4);
      cu_gicp.SetKCorrespondences(12);

      cudaEvent_t start, stop;
      cudaEventCreate(&start);
      cudaEventCreate(&stop);
      cudaEventRecord(start);

      cu_gicp.Align(init, nullptr);
      cudaEventRecord(stop);
      cudaEventSynchronize(stop);
      float temp_milliseconds;
      cudaEventElapsedTime(&temp_milliseconds, start, stop);
      std::cout << "Elapsed time: " << temp_milliseconds << std::endl;
      millisseconds += temp_milliseconds;
      cu_target_point_cloud = cu_source_cloud;
      cu_gicp.GetSourceVectors(target_kd_tree, target_covariances);
      init = cu_gicp.GetFinalTransformation();
    }
    state.SetIterationTime((millisseconds / 10.f));
  }
}

BENCHMARK(BM_cugicp)->Unit(benchmark::kMillisecond)->UseManualTime();

BENCHMARK_MAIN();
