#include <cstdlib>
#include <iostream>
#include <memory>
#include <random>

#include <benchmark/benchmark.h>

#include <boost/cast.hpp>
#include <boost/numeric/conversion/cast.hpp>
#include <pcl/common/transforms.h>
#include <pcl/io/pcd_io.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <thrust/device_vector.h>
#include <sophus/se3.hpp>
#include "pcl/point_cloud.h"

#include "cukdtree/cukdtree.h"
#include "cupointcloud/cupointcloud.h"
#include "curegistration/cugicp.h"
#include "curvature/curvature.h"
#include "kitti_devkit/kitti_util.h"
#include "viewer/viewer.h"

struct is_nan {
  __host__ __device__ bool operator()(const perl_registration::cuPointXYZ p) {
    return (isnan(p.x) || isnan(p.y) || isnan(p.z));
  }
};
int main(int argc, char** argv) {
  std::unique_ptr<perl_registration::Viewer> viewer;
  // change this to run viewer
  constexpr bool run_viewer = true;
  if (run_viewer) {
    viewer = std::make_unique<perl_registration::Viewer>();
  }

  std::string sequence_path =
      "/home/sparki/data/kitti/dataset/sequences/00/velodyne/";

  char buff1[100];
  int cloud_indx = std::atoi(argv[1]);
  snprintf(buff1, sizeof(buff1), "%06d.bin", cloud_indx);
  std::string file1 = sequence_path + std::string(buff1);

  pcl::PointCloud<pcl::PointXYZ> source_cloud = kitti2pcl(file1);
  /*
  std::string file1 = //"/home/sparki/Downloads/source_pcd(2).pcd";
  "/home/sparki/data/kitti_vo/sequence_5_point_cloud/000003.pcd";
  pcl::PCDReader p;
  pcl::PCLPointCloud2 blob;
  Eigen::Vector4f origin;
  Eigen::Quaternionf orientation;
  int pcd_type;

  pcl::PointCloud<pcl::PointXYZ> source_cloud;
  if (p.read(file1, blob, origin, orientation, pcd_type, 0) == -1)
  //* load the file
  {
    PCL_ERROR("Couldn't read target pcd \n");
    return (-1);
  }
  pcl::fromPCLPointCloud2(blob, source_cloud);
  */
  std::cout << "Source size: " << source_cloud.size() << std::endl;
  auto cu_source_cloud = std::make_shared<
      perl_registration::cuPointCloud<perl_registration::cuPointXYZ>>(
      source_cloud);
  cu_source_cloud->points.erase(
      thrust::remove_if(cu_source_cloud->begin(), cu_source_cloud->end(),
                        is_nan()),
      cu_source_cloud->end());

  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);
  cudaEventRecord(start);

  perl_registration::Curvature curv;
  curv.SetKCorrespondences(20);
  curv.SetInputSource(cu_source_cloud);
  auto cu_curvature = std::make_shared<
      perl_registration::cuPointCloud<perl_registration::cuPointXYZI>>();
  curv.Compute(cu_curvature);

  thrust::host_vector<perl_registration::cuPointXYZI> host_vec(
      cu_curvature->points);
  pcl::PointCloud<pcl::PointXYZI> host_curvature;
  for (auto p : host_vec) {
    pcl::PointXYZI pt;
    pt.x = p.x;
    pt.y = p.y;
    pt.z = p.z;
    pt.intensity = p.intensity;
    host_curvature.push_back(pt);
  };

  cudaEventRecord(stop);
  cudaEventSynchronize(stop);
  float temp_milliseconds;
  cudaEventElapsedTime(&temp_milliseconds, start, stop);
  std::cout << "Elapsed time: " << temp_milliseconds << std::endl;

  // Viewer stuff
  if (run_viewer) {
    viewer->addPointCloudIntensity(host_curvature, 0.0, 0.5, file1);
  }

  if (run_viewer) {
    while (!viewer->wasStopped()) {
      std::this_thread::sleep_for(std::chrono::microseconds(1000000));
    }
  }
  return 0;
}
