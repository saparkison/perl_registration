#include <iostream>
#include <memory>
#include <random>

#include <benchmark/benchmark.h>

#include <pcl/common/transforms.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <thrust/device_vector.h>
#include <sophus/se3.hpp>
#include "pcl/point_cloud.h"

#include "cukdtree/cukdtree.h"
#include "cupointcloud/cupointcloud.h"
#include "curegistration/cugicp.h"
#include "kitti_devkit/kitti_util.h"
#include "viewer/viewer.h"

int main() {
  float millisseconds = 0;

  int sizes[11] = {4540, 1100, 4660, 800,  270, 2760,
                   1100, 1100, 4070, 1590, 1200};

  perl_registration::cuPointCloud<perl_registration::cuPointXYZ>::SharedPtr
      cu_target_point_cloud;
  perl_registration::cuKdTree<perl_registration::cuPointXYZ>::SharedPtr
      target_kd_tree;
  std::shared_ptr<thrust::device_vector<Eigen::Matrix3f>> target_covariances;

  std::unique_ptr<perl_registration::Viewer> viewer;
  // change this to run viewer
  constexpr bool run_viewer = true;
  if (run_viewer) {
    viewer = std::make_unique<perl_registration::Viewer>();
  }

  std::string sequence_path =
      "/home/sparki/data/kitti/dataset/sequences/00/velodyne/";
  std::ofstream results_file("/home/sparki/data/kitti/dataset/results/00.txt");
  std::ifstream calib_file(
      "/home/sparki/data/kitti/dataset/sequences/00/calib.txt");
  Eigen::Matrix4f calib_mat = Eigen::Matrix4f::Identity();
  calib_mat.block<3, 4>(0, 0) =
      ReadCalibrationVariable<3, 4>("Tr:", calib_file);
  std::cout << calib_mat << std::endl;
  Sophus::SE3f Lidar_to_camera = Sophus::SE3f::fitToSE3(calib_mat);
  calib_file.close();

  Sophus::SE3f init;
  Sophus::SE3f toInitial;
  PoseToFile(toInitial, results_file);
  for (int i = 0; i < sizes[0]; i += 1) {
    char buff1[100];
    char buff2[100];
    snprintf(buff1, sizeof(buff1), "%06d.bin", i);
    snprintf(buff2, sizeof(buff2), "%06d.bin", i + 1);
    std::string file1 = sequence_path + std::string(buff1);
    std::string file2 = sequence_path + std::string(buff2);

    perl_registration::cuGICP cu_gicp;
    if (i == 0) {
      pcl::PointCloud<pcl::PointXYZ> target_cloud = kitti2pcl(file1);
      pcl::transformPointCloud(target_cloud, target_cloud,
                               Lidar_to_camera.matrix());
      std::cout << "Target size: " << target_cloud.size() << std::endl;
      if (run_viewer) {
        viewer->addPointCloudSingleColor(target_cloud, 1, 0, 0, file1);
      }
      cu_target_point_cloud = std::make_shared<
          perl_registration::cuPointCloud<perl_registration::cuPointXYZ>>(
          target_cloud);
      cu_gicp.SetInputTarget(cu_target_point_cloud);
    } else {
      cu_gicp.SetInputTarget(cu_target_point_cloud, target_kd_tree,
                             target_covariances);
    }

    pcl::PointCloud<pcl::PointXYZ> source_cloud = kitti2pcl(file2);
    pcl::transformPointCloud(source_cloud, source_cloud,
                             Lidar_to_camera.matrix());
    std::cout << "Source size: " << source_cloud.size() << std::endl;
    auto cu_source_cloud = std::make_shared<
        perl_registration::cuPointCloud<perl_registration::cuPointXYZ>>(
        source_cloud);
    cu_gicp.SetInputSource(cu_source_cloud);
    cu_gicp.SetAlpha(2.0);
    cu_gicp.SetKCorrespondences(20);

    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start);

    cu_gicp.Align(init, nullptr);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    float temp_milliseconds;
    cudaEventElapsedTime(&temp_milliseconds, start, stop);
    std::cout << "Elapsed time: " << temp_milliseconds << std::endl;
    millisseconds += temp_milliseconds;
    init = cu_gicp.GetFinalTransformation();

    // Viewer stuff
    toInitial = toInitial * init;
    PoseToFile(toInitial, results_file);
    if (run_viewer) {
      if (i % 5 == 0) {
        pcl::PointCloud<pcl::PointXYZ> transformed_pc;
        pcl::transformPointCloud(source_cloud, transformed_pc,
                                 toInitial.matrix());
        viewer->addPointCloudSingleColor(transformed_pc, 1, 0, 0, file2);
      }
    }

    // Transfer Source to Target
    cu_target_point_cloud = cu_source_cloud;
    cu_gicp.GetSourceVectors(target_kd_tree, target_covariances);
  }
  if (run_viewer) {
    while (!viewer->wasStopped()) {
      std::this_thread::sleep_for(std::chrono::microseconds(1000000));
    }
  }
  return 0;
}
