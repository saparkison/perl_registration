#include <chrono>
#include <iostream>
#include <memory>
#include <random>

// clang-format off
#include <boost/cast.hpp>
#include <pcl/io/pcd_io.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/point_cloud.h>
#include <thrust/device_vector.h>
#include <sophus/se3.hpp>
// clang-format on

#include "cukdtree/cukdtree.h"
#include "cupointcloud/cupointcloud.h"
#include "curegistration/cugicp.h"
#include "curvature/curvature.h"
#include "viewer/viewer.h"

struct is_nan {
  __host__ __device__ bool operator()(const perl_registration::cuPointXYZ p) {
    return (isnan(p.x) || isnan(p.y) || isnan(p.z));
  }
};
int main() {
  std::string file1 = "../../../../../Downloads/target_pcd(2).pcd";
  //"../../../../../data/kitti_vo/sequence_5_point_cloud/000000.pcd";
  std::string file2 = "../../../../../Downloads/source_pcd(2).pcd";
  //"../../../../../data/kitti_vo/sequence_5_point_cloud/000003.pcd";

  pcl::PCDReader p;
  pcl::PCLPointCloud2 blob;
  Eigen::Vector4f origin;
  Eigen::Quaternionf orientation;
  int pcd_type;

  pcl::PointCloud<pcl::PointXYZ> target_cloud;
  if (p.read(file1, blob, origin, orientation, pcd_type, 0) == -1)
  //* load the file
  {
    PCL_ERROR("Couldn't read target pcd \n");
    return (-1);
  }
  pcl::fromPCLPointCloud2(blob, target_cloud);
  auto cu_target_cloud = std::make_shared<
      perl_registration::cuPointCloud<perl_registration::cuPointXYZ>>(
      target_cloud);
  cu_target_cloud->points.erase(
      thrust::remove_if(cu_target_cloud->begin(), cu_target_cloud->end(),
                        is_nan()),
      cu_target_cloud->end());
  pcl::PointCloud<pcl::PointXYZ> source_cloud;
  if (p.read(file2, blob, origin, orientation, pcd_type, 0) == -1)
  //* load the file
  {
    PCL_ERROR("Couldn't read target pcd \n");
    return (-1);
  }
  pcl::fromPCLPointCloud2(blob, source_cloud);
  auto cu_source_cloud = std::make_shared<
      perl_registration::cuPointCloud<perl_registration::cuPointXYZ>>(
      source_cloud);
  cu_source_cloud->points.erase(
      thrust::remove_if(cu_source_cloud->begin(), cu_source_cloud->end(),
                        is_nan()),
      cu_source_cloud->end());

  std::cout << "Source Points: " << cu_source_cloud->size() << std::endl;

  auto t1 = std::chrono::high_resolution_clock::now();
  perl_registration::cuGICP cu_gicp;
  cu_gicp.SetKCorrespondences(12);
  cu_gicp.SetAlpha(4);
  cu_gicp.SetInputSource(cu_source_cloud);
  cu_gicp.SetInputTarget(cu_target_cloud);

  Sophus::SE3f init;
  cu_gicp.Align(init, nullptr);
  auto t2 = std::chrono::high_resolution_clock::now();
  std::cout
      << "Run Time: "
      << std::chrono::duration_cast<std::chrono::milliseconds>(t2 - t1).count()
      << std::endl;

  auto t3 = std::chrono::high_resolution_clock::now();
  perl_registration::Curvature curv;
  curv.SetInputSource(cu_source_cloud);
  auto cu_curvature = std::make_shared<
      perl_registration::cuPointCloud<perl_registration::cuPointXYZI>>();
  curv.Compute(cu_curvature);
  auto t4 = std::chrono::high_resolution_clock::now();
  std::cout
      << "Curvature Run Time: "
      << std::chrono::duration_cast<std::chrono::milliseconds>(t4 - t3).count()
      << std::endl;

  return 0;
}
