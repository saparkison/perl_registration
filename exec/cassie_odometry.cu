#include <iostream>
#include <memory>
#include <random>

#include "viewer/viewer.h"

#include <boost/cast.hpp>
#include <boost/numeric/conversion/cast.hpp>
#include <pcl/common/transforms.h>
#include <pcl/io/pcd_io.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <thrust/device_vector.h>
#include <sophus/se3.hpp>
#include "pcl/point_cloud.h"

#include "cukdtree/cukdtree.h"
#include "cupointcloud/cupointcloud.h"
#include "curegistration/cugicp.h"
#include "kitti_devkit/kitti_util.h"

int main() {
  float millisseconds = 0;

  perl_registration::cuPointCloud<perl_registration::cuPointXYZ>::SharedPtr
      cu_target_point_cloud;
  perl_registration::cuKdTree<perl_registration::cuPointXYZ>::SharedPtr
      target_kd_tree;
  std::shared_ptr<thrust::device_vector<Eigen::Matrix3f>> target_covariances;

  std::unique_ptr<perl_registration::Viewer> viewer;
  // change this to run viewer
  constexpr bool run_viewer = true;
  if (run_viewer) {
    viewer = std::make_unique<perl_registration::Viewer>();
  }

  std::string sequence_path = "/home/sparki/data/cassie/";

  pcl::PCDReader p;
  pcl::PCLPointCloud2 blob;
  Eigen::Vector4f origin;
  Eigen::Quaternionf orientation;
  int pcd_type;

  Sophus::SE3f init;
  Sophus::SE3f toInitial;
  int start = 40;
  int stop = 50;
  // int stop = 218;
  for (int i = start; i < stop; i += 1) {
    char buff1[100];
    char buff2[100];
    snprintf(buff1, sizeof(buff1), "%d.pcd", i);
    snprintf(buff2, sizeof(buff2), "%d.pcd", i + 1);
    std::string file1 = sequence_path + std::string(buff1);
    std::string file2 = sequence_path + std::string(buff2);

    perl_registration::cuGICP cu_gicp;
    if (i == start) {
      std::cout << "Reading " << file1 << std::endl;
      if (p.read(file1, blob, origin, orientation, pcd_type, 0) == -1)
      //* load the file
      {
        PCL_ERROR("Couldn't read target pcd \n");
        return (-1);
      }
      pcl::PointCloud<pcl::PointXYZ> target_cloud;
      pcl::fromPCLPointCloud2(blob, target_cloud);
      std::cout << "Target size: " << target_cloud.size() << std::endl;
      if (run_viewer) {
        viewer->addPointCloudSingleColor(target_cloud, 1, 0, 0, file1);
      }
      cu_target_point_cloud = std::make_shared<
          perl_registration::cuPointCloud<perl_registration::cuPointXYZ>>(
          target_cloud);
      cu_gicp.SetInputTarget(cu_target_point_cloud);
    } else {
      cu_gicp.SetInputTarget(cu_target_point_cloud, target_kd_tree,
                             target_covariances);
    }

    std::cout << "Reading " << file2 << std::endl;
    if (p.read(file2, blob, origin, orientation, pcd_type, 0) == -1)
    //* load the file
    {
      PCL_ERROR("Couldn't read target pcd \n");
      return (-1);
    }
    pcl::PointCloud<pcl::PointXYZ> source_cloud;
    pcl::fromPCLPointCloud2(blob, source_cloud);
    std::cout << "Source size: " << source_cloud.size() << std::endl;
    auto cu_source_cloud = std::make_shared<
        perl_registration::cuPointCloud<perl_registration::cuPointXYZ>>(
        source_cloud);
    cu_gicp.SetInputSource(cu_source_cloud);
    cu_gicp.SetAlpha(1.2);
    cu_gicp.SetKCorrespondences(30);

    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start);

    cu_gicp.Align(init, nullptr);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    float temp_milliseconds;
    cudaEventElapsedTime(&temp_milliseconds, start, stop);
    std::cout << "Elapsed time: " << temp_milliseconds << std::endl;
    millisseconds += temp_milliseconds;
    init = cu_gicp.GetFinalTransformation();

    // Viewer stuff
    toInitial = toInitial * init;
    if (run_viewer) {
      pcl::PointCloud<pcl::PointXYZ> transformed_pc;
      pcl::transformPointCloud(source_cloud, transformed_pc,
                               toInitial.matrix());
      viewer->addPointCloudSingleColor(transformed_pc, 1, 0, 0, file2);
    }

    // Transfer Source to Target
    cu_target_point_cloud = cu_source_cloud;
    cu_gicp.GetSourceVectors(target_kd_tree, target_covariances);
  }
  if (run_viewer) {
    while (!viewer->wasStopped()) {
      std::this_thread::sleep_for(std::chrono::microseconds(1000000));
    }
  }
  return 0;
}
