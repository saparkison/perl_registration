file (GLOB HEADER_FILES *.h)

set(SRC_OD
    cpp/evaluate_odometry.cpp
    cpp/matrix.cpp
    cpp/matrix.h
    cpp/mail.h
    )

add_executable(evaluate_odometry ${SRC_OD})

set(SRC_CO
    cpp/evaluate_convergence.cpp
    cpp/matrix.cpp
    cpp/matrix.h
    cpp/mail.h
    )

  add_executable(evaluate_convergence ${SRC_CO})

install(FILES ${HEADER_FILES} DESTINATION "include/kitti_devkit/")
