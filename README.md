# About

**Name:** PeRL Registration Library

**Summary:** A Parallelized algorithm to solve the Generalized ICP cost function

**Description:** A Parallelized algorithm to solve the Generalized ICP cost function 


**License:** BSD 3-Clause

# Requirements


## libraries
* OpenMP 4.0
* CUDA 9.1
* Thrust
* Eigen 3.3.90 ("default" branch)
* Ceres-Solver 1.14
* PCL ("master" branch)
* gtest
* Sophus
* benchmark

# Install

**Local:** `make`. The project will be installed to `build/`.

**System-wide:** `make BUILD_PREFIX=/usr/local` as root.

# Maintainers

Steven Parkison (sparki@umich.edu), Arash Ushani (aushani@umich.edu), Sudhanva
Sreesha (ssreesha@umich.edu)
